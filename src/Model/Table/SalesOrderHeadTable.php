<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SalesOrderHead Model
 *
 * @method \App\Model\Entity\SalesOrderHead get($primaryKey, $options = [])
 * @method \App\Model\Entity\SalesOrderHead newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SalesOrderHead[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SalesOrderHead|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SalesOrderHead patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SalesOrderHead[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SalesOrderHead findOrCreate($search, callable $callback = null, $options = [])
 */
class SalesOrderHeadTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('sales_order_head');
        $this->displayField('Sales_Order_Code');
        $this->primaryKey('Sales_Order_Code');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('Sales_Order_Code', 'create');

        $validator
            ->integer('Company_IdNo')
            ->requirePresence('Company_IdNo', 'create')
            ->notEmpty('Company_IdNo');

        $validator
            ->requirePresence('Sales_Order_No', 'create')
            ->notEmpty('Sales_Order_No');

        $validator
            ->decimal('for_OrderBy')
            ->requirePresence('for_OrderBy', 'create')
            ->notEmpty('for_OrderBy');

        $validator
            ->dateTime('Sales_Order_Date')
            ->requirePresence('Sales_Order_Date', 'create')
            ->notEmpty('Sales_Order_Date');

        $validator
            ->allowEmpty('Payment_Method');

        $validator
            ->integer('Ledger_IdNo')
            ->allowEmpty('Ledger_IdNo');

        $validator
            ->allowEmpty('Cash_PartyName');

        $validator
            ->allowEmpty('Party_PhoneNo');

        $validator
            ->integer('SalesAc_IdNo')
            ->allowEmpty('SalesAc_IdNo');

        $validator
            ->allowEmpty('Tax_Type');

        $validator
            ->integer('TaxAc_IdNo')
            ->allowEmpty('TaxAc_IdNo');

        $validator
            ->allowEmpty('Delivery_Address1');

        $validator
            ->allowEmpty('Delivery_Address2');

        $validator
            ->allowEmpty('Delivery_Address3');

        $validator
            ->allowEmpty('Vehicle_No');

        $validator
            ->allowEmpty('Narration');

        $validator
            ->decimal('Total_Qty')
            ->allowEmpty('Total_Qty');

        $validator
            ->integer('Total_Bags')
            ->allowEmpty('Total_Bags');

        $validator
            ->decimal('SubTotal_Amount')
            ->allowEmpty('SubTotal_Amount');

        $validator
            ->decimal('Total_DiscountAmount')
            ->allowEmpty('Total_DiscountAmount');

        $validator
            ->decimal('Total_TaxAmount')
            ->allowEmpty('Total_TaxAmount');

        $validator
            ->decimal('Gross_Amount')
            ->allowEmpty('Gross_Amount');

        $validator
            ->decimal('CashDiscount_Perc')
            ->allowEmpty('CashDiscount_Perc');

        $validator
            ->decimal('CashDiscount_Amount')
            ->allowEmpty('CashDiscount_Amount');

        $validator
            ->decimal('Assessable_Value')
            ->allowEmpty('Assessable_Value');

        $validator
            ->decimal('Tax_Perc')
            ->allowEmpty('Tax_Perc');

        $validator
            ->decimal('Tax_Amount')
            ->allowEmpty('Tax_Amount');

        $validator
            ->decimal('Freight_Amount')
            ->allowEmpty('Freight_Amount');

        $validator
            ->decimal('AddLess_Amount')
            ->allowEmpty('AddLess_Amount');

        $validator
            ->decimal('Round_Off')
            ->allowEmpty('Round_Off');

        $validator
            ->decimal('Net_Amount')
            ->allowEmpty('Net_Amount');

        $validator
            ->allowEmpty('Document_Through');

        $validator
            ->allowEmpty('Despatch_To');

        $validator
            ->allowEmpty('Lr_No');

        $validator
            ->allowEmpty('Lr_Date');

        $validator
            ->allowEmpty('Booked_By');

        $validator
            ->integer('Transport_IdNo')
            ->allowEmpty('Transport_IdNo');

        $validator
            ->decimal('Freight_ToPay_Amount')
            ->allowEmpty('Freight_ToPay_Amount');

        $validator
            ->allowEmpty('Dc_No');

        $validator
            ->allowEmpty('Dc_Date');

        $validator
            ->allowEmpty('Order_No');

        $validator
            ->allowEmpty('Order_Date');

        $validator
            ->integer('Against_CForm_Status')
            ->allowEmpty('Against_CForm_Status');

        $validator
            ->allowEmpty('Entry_Type');

        $validator
            ->allowEmpty('Payment_Terms');

        $validator
            ->integer('OnAc_IdNo')
            ->allowEmpty('OnAc_IdNo');

        $validator
            ->decimal('Extra_Charges')
            ->allowEmpty('Extra_Charges');

        $validator
            ->decimal('Total_Extra_Copies')
            ->allowEmpty('Total_Extra_Copies');

        $validator
            ->decimal('Sub_Total_Copies')
            ->allowEmpty('Sub_Total_Copies');

        $validator
            ->allowEmpty('Party_Name');

        $validator
            ->decimal('Weight')
            ->allowEmpty('Weight');

        $validator
            ->integer('Sales_OrderAc_IdNo')
            ->allowEmpty('Sales_OrderAc_IdNo');

        $validator
            ->integer('Order_Close')
            ->allowEmpty('Order_Close');

        $validator
            ->allowEmpty('Sales_Order_Selection_Code');

        $validator
            ->allowEmpty('Quotation_No');

        $validator
            ->allowEmpty('Quotation_Date');

        $validator
            ->integer('Salesman_IdNo')
            ->allowEmpty('Salesman_IdNo');

        $validator
            ->allowEmpty('Removal_Date');

        $validator
            ->allowEmpty('Date_Time_Of_Supply');

        $validator
            ->decimal('Total_DiscountAmount_item')
            ->allowEmpty('Total_DiscountAmount_item');

        $validator
            ->allowEmpty('Entry_VAT_GST_Type');

        $validator
            ->allowEmpty('Entry_GST_Tax_Type');

        $validator
            ->decimal('CGst_Amount')
            ->allowEmpty('CGst_Amount');

        $validator
            ->decimal('SGst_Amount')
            ->allowEmpty('SGst_Amount');

        $validator
            ->decimal('IGst_Amount')
            ->allowEmpty('IGst_Amount');

        $validator
            ->integer('DeliveryTo_IdNo')
            ->allowEmpty('DeliveryTo_IdNo');

        return $validator;
    }
}
