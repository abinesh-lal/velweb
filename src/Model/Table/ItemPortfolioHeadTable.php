<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ItemPortfolioHead Model
 *
 * @method \App\Model\Entity\ItemPortfolioHead get($primaryKey, $options = [])
 * @method \App\Model\Entity\ItemPortfolioHead newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ItemPortfolioHead[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ItemPortfolioHead|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ItemPortfolioHead patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ItemPortfolioHead[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ItemPortfolioHead findOrCreate($search, callable $callback = null, $options = [])
 */
class ItemPortfolioHeadTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('item_portfolio_head');
        $this->displayField('ItemPortfolio_IdNo');
        $this->primaryKey('ItemPortfolio_IdNo');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('ItemPortfolio_IdNo')
            ->allowEmpty('ItemPortfolio_IdNo', 'create');

        $validator
            ->requirePresence('ItemPortfolio_FolderNameList', 'create')
            ->notEmpty('ItemPortfolio_FolderNameList');

        $validator
            ->integer('Item_idNo')
            ->requirePresence('Item_idNo', 'create')
            ->notEmpty('Item_idNo');

        return $validator;
    }
}
