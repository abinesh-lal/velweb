<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ItemGroupHead Model
 *
 * @method \App\Model\Entity\ItemGroupHead get($primaryKey, $options = [])
 * @method \App\Model\Entity\ItemGroupHead newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ItemGroupHead[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ItemGroupHead|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ItemGroupHead patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ItemGroupHead[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ItemGroupHead findOrCreate($search, callable $callback = null, $options = [])
 */
class ItemGroupHeadTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('ItemGroup_Head');
        $this->displayField('ItemGroup_IdNo');
        $this->primaryKey('ItemGroup_IdNo');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('ItemGroup_IdNo')
            ->allowEmpty('ItemGroup_IdNo', 'create');

        $validator
            ->requirePresence('ItemGroup_Name', 'create')
            ->notEmpty('ItemGroup_Name');

        $validator
            ->requirePresence('Sur_Name', 'create')
            ->notEmpty('Sur_Name')
            ->add('Sur_Name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmpty('Commodity_Code');

        $validator
            ->allowEmpty('Item_HSN_Code');

        $validator
            ->decimal('Item_GST_Percentage')
            ->allowEmpty('Item_GST_Percentage');

        $validator
            ->integer('Cetegory_IdNo')
            ->allowEmpty('Cetegory_IdNo');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['Sur_Name']));

        return $rules;
    }
}
