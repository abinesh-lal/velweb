<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ItemHead Model
 *
 * @method \App\Model\Entity\ItemHead get($primaryKey, $options = [])
 * @method \App\Model\Entity\ItemHead newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ItemHead[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ItemHead|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ItemHead patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ItemHead[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ItemHead findOrCreate($search, callable $callback = null, $options = [])
 */
class ItemHeadTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('item_head');
        $this->displayField('Item_IdNo');
        $this->primaryKey('Item_IdNo');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('Item_IdNo')
            ->allowEmpty('Item_IdNo', 'create');

        $validator
            ->requirePresence('Item_Name', 'create')
            ->notEmpty('Item_Name');

        $validator
            ->requirePresence('Sur_Name', 'create')
            ->notEmpty('Sur_Name')
            ->add('Sur_Name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmpty('Item_Code');

        $validator
            ->integer('ItemGroup_IdNo')
            ->allowEmpty('ItemGroup_IdNo');

        $validator
            ->integer('Unit_IdNo')
            ->allowEmpty('Unit_IdNo');

        $validator
            ->decimal('Tax_Percentage')
            ->allowEmpty('Tax_Percentage');

        $validator
            ->decimal('Sale_TaxRate')
            ->allowEmpty('Sale_TaxRate');

        $validator
            ->decimal('Sales_Rate')
            ->allowEmpty('Sales_Rate');

        $validator
            ->decimal('Cost_Rate')
            ->allowEmpty('Cost_Rate');

        $validator
            ->decimal('Minimum_Stock')
            ->allowEmpty('Minimum_Stock');

        $validator
            ->integer('Price_List_IdNo')
            ->allowEmpty('Price_List_IdNo');

        $validator
            ->integer('Ledger_IdNo')
            ->allowEmpty('Ledger_IdNo');

        $validator
            ->allowEmpty('Item_Name_Tamil');

        $validator
            ->allowEmpty('Item_Description');

        $validator
            ->decimal('Gst_Percentage')
            ->allowEmpty('Gst_Percentage');

        $validator
            ->decimal('Gst_Rate')
            ->allowEmpty('Gst_Rate');

        $validator
            ->integer('Job_Work_Status')
            ->allowEmpty('Job_Work_Status');

        $validator
            ->decimal('MRP_Rate')
            ->allowEmpty('MRP_Rate');

        $validator
            ->decimal('Discount_Percentage')
            ->allowEmpty('Discount_Percentage');

        $validator
            ->allowEmpty('Rack_No');

        $validator
            ->decimal('Sales_Profit_Retail')
            ->allowEmpty('Sales_Profit_Retail');

        $validator
            ->decimal('Sales_Rate_Retail')
            ->allowEmpty('Sales_Rate_Retail');

        $validator
            ->decimal('Sales_Profit_Wholesale')
            ->allowEmpty('Sales_Profit_Wholesale');

        $validator
            ->decimal('Sales_Rate_Wholesale')
            ->allowEmpty('Sales_Rate_Wholesale');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['Sur_Name']));

        return $rules;
    }
}
