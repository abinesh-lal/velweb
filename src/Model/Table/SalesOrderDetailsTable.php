<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SalesOrderDetails Model
 *
 * @method \App\Model\Entity\SalesOrderDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\SalesOrderDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SalesOrderDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SalesOrderDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SalesOrderDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SalesOrderDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SalesOrderDetail findOrCreate($search, callable $callback = null, $options = [])
 */
class SalesOrderDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('sales_order_details');
        $this->displayField('Sales_Order_Code');
        $this->primaryKey(['Sales_Order_Code', 'SL_No']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('Sales_Order_Code', 'create');

        $validator
            ->integer('Company_IdNo')
            ->requirePresence('Company_IdNo', 'create')
            ->notEmpty('Company_IdNo');

        $validator
            ->requirePresence('Sales_Order_No', 'create')
            ->notEmpty('Sales_Order_No');

        $validator
            ->decimal('for_OrderBy')
            ->requirePresence('for_OrderBy', 'create')
            ->notEmpty('for_OrderBy');

        $validator
            ->dateTime('Sales_Order_Date')
            ->requirePresence('Sales_Order_Date', 'create')
            ->notEmpty('Sales_Order_Date');

        $validator
            ->integer('Ledger_IdNo')
            ->requirePresence('Ledger_IdNo', 'create')
            ->notEmpty('Ledger_IdNo');

        $validator
            ->integer('SL_No')
            ->notEmpty('SL_No', 'create');

        $validator
            ->integer('Item_IdNo')
            ->allowEmpty('Item_IdNo');

        $validator
            ->integer('ItemGroup_IdNo')
            ->allowEmpty('ItemGroup_IdNo');

        $validator
            ->integer('Unit_IdNo')
            ->allowEmpty('Unit_IdNo');

        $validator
            ->decimal('Noof_Items')
            ->allowEmpty('Noof_Items');

        $validator
            ->integer('Bags')
            ->allowEmpty('Bags');

        $validator
            ->decimal('Weight_Bag')
            ->allowEmpty('Weight_Bag');

        $validator
            ->decimal('Weight')
            ->allowEmpty('Weight');

        $validator
            ->decimal('Rate')
            ->allowEmpty('Rate');

        $validator
            ->decimal('Tax_Rate')
            ->allowEmpty('Tax_Rate');

        $validator
            ->decimal('Amount')
            ->allowEmpty('Amount');

        $validator
            ->decimal('Discount_Perc')
            ->allowEmpty('Discount_Perc');

        $validator
            ->decimal('Discount_Amount')
            ->allowEmpty('Discount_Amount');

        $validator
            ->decimal('Tax_Perc')
            ->allowEmpty('Tax_Perc');

        $validator
            ->decimal('Tax_Amount')
            ->allowEmpty('Tax_Amount');

        $validator
            ->decimal('Total_Amount')
            ->allowEmpty('Total_Amount');

        $validator
            ->allowEmpty('Bag_Nos');

        $validator
            ->allowEmpty('Serial_No');

        $validator
            ->integer('Size_IdNo')
            ->allowEmpty('Size_IdNo');

        $validator
            ->decimal('Meters')
            ->allowEmpty('Meters');

        $validator
            ->integer('Colour_IdNo')
            ->allowEmpty('Colour_IdNo');

        $validator
            ->decimal('Noof_Items_Return')
            ->allowEmpty('Noof_Items_Return');

        $validator
            ->integer('Sales_Order_Detail_SlNo')
            ->requirePresence('Sales_Order_Detail_SlNo', 'create')
            ->notEmpty('Sales_Order_Detail_SlNo');

        $validator
            ->decimal('Sales_Items')
            ->allowEmpty('Sales_Items');

        $validator
            ->allowEmpty('Sales_Quotation_Code');

        $validator
            ->integer('Sales_Quotation_Detail_SlNo')
            ->allowEmpty('Sales_Quotation_Detail_SlNo');

        $validator
            ->allowEmpty('item_Description');

        $validator
            ->allowEmpty('Entry_Type');

        $validator
            ->decimal('Scheme_Disc_Percentage')
            ->allowEmpty('Scheme_Disc_Percentage');

        $validator
            ->decimal('Scheme_Discount')
            ->allowEmpty('Scheme_Discount');

        $validator
            ->decimal('Trade_Disc_Percentage')
            ->allowEmpty('Trade_Disc_Percentage');

        $validator
            ->decimal('Trade_Discount')
            ->allowEmpty('Trade_Discount');

        $validator
            ->decimal('Cgst_Percentage')
            ->allowEmpty('Cgst_Percentage');

        $validator
            ->decimal('Cgst_Amount')
            ->allowEmpty('Cgst_Amount');

        $validator
            ->decimal('Sgst_Percentage')
            ->allowEmpty('Sgst_Percentage');

        $validator
            ->decimal('Sgst_Amount')
            ->allowEmpty('Sgst_Amount');

        $validator
            ->decimal('Igst_Percentage')
            ->allowEmpty('Igst_Percentage');

        $validator
            ->decimal('Igst_Amount')
            ->allowEmpty('Igst_Amount');

        $validator
            ->decimal('Net_Amount')
            ->allowEmpty('Net_Amount');

        $validator
            ->decimal('Total_Rate')
            ->allowEmpty('Total_Rate');

        $validator
            ->decimal('Scheme_UCP')
            ->allowEmpty('Scheme_UCP');

        $validator
            ->decimal('Discount_Total')
            ->allowEmpty('Discount_Total');

        $validator
            ->decimal('Cash_Discount_Perc_For_All_Item')
            ->allowEmpty('Cash_Discount_Perc_For_All_Item');

        $validator
            ->decimal('Cash_Discount_Amount_For_All_Item')
            ->allowEmpty('Cash_Discount_Amount_For_All_Item');

        $validator
            ->allowEmpty('HSN_Code');

        $validator
            ->decimal('Assessable_Value')
            ->allowEmpty('Assessable_Value');

        $validator
            ->decimal('Cancel_Quantity')
            ->allowEmpty('Cancel_Quantity');

        return $validator;
    }
}
