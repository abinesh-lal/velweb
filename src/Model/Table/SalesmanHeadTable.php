<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SalesmanHead Model
 *
 * @method \App\Model\Entity\SalesmanHead get($primaryKey, $options = [])
 * @method \App\Model\Entity\SalesmanHead newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SalesmanHead[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SalesmanHead|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SalesmanHead patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SalesmanHead[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SalesmanHead findOrCreate($search, callable $callback = null, $options = [])
 */
class SalesmanHeadTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('salesman_head');
        $this->displayField('Salesman_Idno');
        $this->primaryKey('Salesman_Idno');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('Salesman_Idno')
            ->allowEmpty('Salesman_Idno', 'create');

        $validator
            ->allowEmpty('Salesman_Name');

        $validator
            ->requirePresence('Sur_Name', 'create')
            ->notEmpty('Sur_Name');

        $validator
            ->allowEmpty('SalesMan_EmailID2');

        $validator
            ->allowEmpty('SalesMan_EmailID');

        $validator
            ->decimal('Collection_Amount')
            ->allowEmpty('Collection_Amount');

        $validator
            ->decimal('Total_Amount')
            ->allowEmpty('Total_Amount');

        $validator
            ->decimal('Total_Quantity')
            ->allowEmpty('Total_Quantity');

        $validator
            ->integer('Month_Idno')
            ->allowEmpty('Month_Idno');

        return $validator;
    }
}
