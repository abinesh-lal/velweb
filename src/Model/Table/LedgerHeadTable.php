<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * LedgerHead Model
 *
 * @method \App\Model\Entity\LedgerHead get($primaryKey, $options = [])
 * @method \App\Model\Entity\LedgerHead newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\LedgerHead[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\LedgerHead|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LedgerHead patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\LedgerHead[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\LedgerHead findOrCreate($search, callable $callback = null, $options = [])
 */
class LedgerHeadTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('ledger_head');
        $this->displayField('Ledger_IdNo');
        $this->primaryKey('Ledger_IdNo');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('Ledger_IdNo')
            ->allowEmpty('Ledger_IdNo', 'create');

        $validator
            ->requirePresence('Ledger_Name', 'create')
            ->notEmpty('Ledger_Name');

        $validator
            ->requirePresence('Sur_Name', 'create')
            ->notEmpty('Sur_Name')
            ->add('Sur_Name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->requirePresence('Ledger_MainName', 'create')
            ->notEmpty('Ledger_MainName');

        $validator
            ->allowEmpty('Ledger_AlaisName');

        $validator
            ->integer('Area_IdNo')
            ->allowEmpty('Area_IdNo');

        $validator
            ->integer('AccountsGroup_IdNo')
            ->allowEmpty('AccountsGroup_IdNo');

        $validator
            ->allowEmpty('Parent_Code');

        $validator
            ->allowEmpty('Bill_Type');

        $validator
            ->allowEmpty('Ledger_Address1');

        $validator
            ->allowEmpty('Ledger_Address2');

        $validator
            ->allowEmpty('Ledger_Address3');

        $validator
            ->allowEmpty('Ledger_Address4');

        $validator
            ->allowEmpty('Ledger_PhoneNo');

        $validator
            ->allowEmpty('Ledger_TinNo');

        $validator
            ->allowEmpty('Ledger_CstNo');

        $validator
            ->allowEmpty('Ledger_Type');

        $validator
            ->allowEmpty('Ledger_EmailID');

        $validator
            ->integer('Price_List_IdNo')
            ->allowEmpty('Price_List_IdNo');

        $validator
            ->allowEmpty('Pan_No');

        $validator
            ->decimal('Rent_Machine')
            ->allowEmpty('Rent_Machine');

        $validator
            ->integer('Free_Copies_Machine')
            ->allowEmpty('Free_Copies_Machine');

        $validator
            ->decimal('Rate_Extra_Copy')
            ->allowEmpty('Rate_Extra_Copy');

        $validator
            ->integer('Machine_IdNo')
            ->allowEmpty('Machine_IdNo');

        $validator
            ->integer('Opening_Reading')
            ->allowEmpty('Opening_Reading');

        $validator
            ->integer('Total_Machine')
            ->allowEmpty('Total_Machine');

        $validator
            ->integer('State_Idno')
            ->allowEmpty('State_Idno');

        $validator
            ->integer('LedgerGroup_Idno')
            ->allowEmpty('LedgerGroup_Idno');

        $validator
            ->decimal('Rate_For_1000')
            ->allowEmpty('Rate_For_1000');

        $validator
            ->decimal('Minimum_Pcs')
            ->allowEmpty('Minimum_Pcs');

        $validator
            ->decimal('Minimum_Bill_Amount')
            ->allowEmpty('Minimum_Bill_Amount');

        $validator
            ->allowEmpty('Ledger_GSTinNo');

        $validator
            ->allowEmpty('Ledger_PanNo');

        $validator
            ->allowEmpty('Owner_Name');

        $validator
            ->dateTime('Birth_Date')
            ->allowEmpty('Birth_Date');

        $validator
            ->dateTime('Wedding_Date')
            ->allowEmpty('Wedding_Date');

        $validator
            ->integer('Agent_idNo')
            ->allowEmpty('Agent_idNo');

        $validator
            ->integer('SalesMan_Idno')
            ->allowEmpty('SalesMan_Idno');

        $validator
            ->allowEmpty('Ledger_DealerCode');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['Sur_Name']));

        return $rules;
    }
}
