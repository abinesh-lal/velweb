<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SalesOrderGstTaxDetails Model
 *
 * @method \App\Model\Entity\SalesOrderGstTaxDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\SalesOrderGstTaxDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SalesOrderGstTaxDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SalesOrderGstTaxDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SalesOrderGstTaxDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SalesOrderGstTaxDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SalesOrderGstTaxDetail findOrCreate($search, callable $callback = null, $options = [])
 */
class SalesOrderGstTaxDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('sales_order_gst_tax_details');
        $this->displayField('Sales_Order_Code');
        $this->primaryKey(['Sales_Order_Code', 'Sl_No']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('Sales_Order_Code', 'create');

        $validator
            ->integer('Company_Idno')
            ->requirePresence('Company_Idno', 'create')
            ->notEmpty('Company_Idno');

        $validator
            ->requirePresence('Sales_Order_No', 'create')
            ->notEmpty('Sales_Order_No');

        $validator
            ->decimal('For_OrderBy')
            ->requirePresence('For_OrderBy', 'create')
            ->notEmpty('For_OrderBy');

        $validator
            ->dateTime('Sales_Order_Date')
            ->requirePresence('Sales_Order_Date', 'create')
            ->notEmpty('Sales_Order_Date');

        $validator
            ->integer('Ledger_IdNo')
            ->requirePresence('Ledger_IdNo', 'create')
            ->notEmpty('Ledger_IdNo');

        $validator
            ->integer('Sl_No')
            ->allowEmpty('Sl_No', 'create');

        $validator
            ->allowEmpty('HSN_Code');

        $validator
            ->decimal('Taxable_Amount')
            ->allowEmpty('Taxable_Amount');

        $validator
            ->decimal('CGST_Percentage')
            ->allowEmpty('CGST_Percentage');

        $validator
            ->decimal('CGST_Amount')
            ->allowEmpty('CGST_Amount');

        $validator
            ->decimal('SGST_Percentage')
            ->allowEmpty('SGST_Percentage');

        $validator
            ->decimal('SGST_Amount')
            ->allowEmpty('SGST_Amount');

        $validator
            ->decimal('IGST_Percentage')
            ->allowEmpty('IGST_Percentage');

        $validator
            ->decimal('IGST_Amount')
            ->allowEmpty('IGST_Amount');

        return $validator;
    }
}
