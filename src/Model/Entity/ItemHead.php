<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ItemHead Entity
 *
 * @property int $Item_IdNo
 * @property string $Item_Name
 * @property string $Sur_Name
 * @property string $Item_Code
 * @property int $ItemGroup_IdNo
 * @property int $Unit_IdNo
 * @property float $Tax_Percentage
 * @property float $Sale_TaxRate
 * @property float $Sales_Rate
 * @property float $Cost_Rate
 * @property float $Minimum_Stock
 * @property int $Price_List_IdNo
 * @property int $Ledger_IdNo
 * @property string $Item_Name_Tamil
 * @property string $Item_Description
 * @property float $Gst_Percentage
 * @property float $Gst_Rate
 * @property int $Job_Work_Status
 * @property float $MRP_Rate
 * @property float $Discount_Percentage
 * @property string $Rack_No
 * @property float $Sales_Profit_Retail
 * @property float $Sales_Rate_Retail
 * @property float $Sales_Profit_Wholesale
 * @property float $Sales_Rate_Wholesale
 */
class ItemHead extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'Item_IdNo' => false
    ];
}
