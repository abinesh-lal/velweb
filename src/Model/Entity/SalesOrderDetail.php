<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SalesOrderDetail Entity
 *
 * @property string $Sales_Order_Code
 * @property int $Company_IdNo
 * @property string $Sales_Order_No
 * @property float $for_OrderBy
 * @property \Cake\I18n\Time $Sales_Order_Date
 * @property int $Ledger_IdNo
 * @property int $SL_No
 * @property int $Item_IdNo
 * @property int $ItemGroup_IdNo
 * @property int $Unit_IdNo
 * @property float $Noof_Items
 * @property int $Bags
 * @property float $Weight_Bag
 * @property float $Weight
 * @property float $Rate
 * @property float $Tax_Rate
 * @property float $Amount
 * @property float $Discount_Perc
 * @property float $Discount_Amount
 * @property float $Tax_Perc
 * @property float $Tax_Amount
 * @property float $Total_Amount
 * @property string $Bag_Nos
 * @property string $Serial_No
 * @property int $Size_IdNo
 * @property float $Meters
 * @property int $Colour_IdNo
 * @property float $Noof_Items_Return
 * @property int $Sales_Order_Detail_SlNo
 * @property float $Sales_Items
 * @property string $Sales_Quotation_Code
 * @property int $Sales_Quotation_Detail_SlNo
 * @property string $item_Description
 * @property string $Entry_Type
 * @property float $Scheme_Disc_Percentage
 * @property float $Scheme_Discount
 * @property float $Trade_Disc_Percentage
 * @property float $Trade_Discount
 * @property float $Cgst_Percentage
 * @property float $Cgst_Amount
 * @property float $Sgst_Percentage
 * @property float $Sgst_Amount
 * @property float $Igst_Percentage
 * @property float $Igst_Amount
 * @property float $Net_Amount
 * @property float $Total_Rate
 * @property float $Scheme_UCP
 * @property float $Discount_Total
 * @property float $Cash_Discount_Perc_For_All_Item
 * @property float $Cash_Discount_Amount_For_All_Item
 * @property string $HSN_Code
 * @property float $Assessable_Value
 * @property float $Cancel_Quantity
 */
class SalesOrderDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
    ];
}
