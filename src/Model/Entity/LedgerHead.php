<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LedgerHead Entity
 *
 * @property int $Ledger_IdNo
 * @property string $Ledger_Name
 * @property string $Sur_Name
 * @property string $Ledger_MainName
 * @property string $Ledger_AlaisName
 * @property int $Area_IdNo
 * @property int $AccountsGroup_IdNo
 * @property string $Parent_Code
 * @property string $Bill_Type
 * @property string $Ledger_Address1
 * @property string $Ledger_Address2
 * @property string $Ledger_Address3
 * @property string $Ledger_Address4
 * @property string $Ledger_PhoneNo
 * @property string $Ledger_TinNo
 * @property string $Ledger_CstNo
 * @property string $Ledger_Type
 * @property string $Ledger_EmailID
 * @property int $Price_List_IdNo
 * @property string $Pan_No
 * @property float $Rent_Machine
 * @property int $Free_Copies_Machine
 * @property float $Rate_Extra_Copy
 * @property int $Machine_IdNo
 * @property int $Opening_Reading
 * @property int $Total_Machine
 * @property int $State_Idno
 * @property int $LedgerGroup_Idno
 * @property float $Rate_For_1000
 * @property float $Minimum_Pcs
 * @property float $Minimum_Bill_Amount
 * @property string $Ledger_GSTinNo
 * @property string $Ledger_PanNo
 * @property string $Owner_Name
 * @property \Cake\I18n\Time $Birth_Date
 * @property \Cake\I18n\Time $Wedding_Date
 * @property int $Agent_idNo
 * @property int $SalesMan_Idno
 * @property string $Ledger_DealerCode
 */
class LedgerHead extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'Ledger_IdNo' => false
    ];
}
