<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SalesOrderGstTaxDetail Entity
 *
 * @property string $Sales_Order_Code
 * @property int $Company_Idno
 * @property string $Sales_Order_No
 * @property float $For_OrderBy
 * @property \Cake\I18n\Time $Sales_Order_Date
 * @property int $Ledger_IdNo
 * @property int $Sl_No
 * @property string $HSN_Code
 * @property float $Taxable_Amount
 * @property float $CGST_Percentage
 * @property float $CGST_Amount
 * @property float $SGST_Percentage
 * @property float $SGST_Amount
 * @property float $IGST_Percentage
 * @property float $IGST_Amount
 */
class SalesOrderGstTaxDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'Sales_Order_Code' => false,
        'Sl_No' => false
    ];
}
