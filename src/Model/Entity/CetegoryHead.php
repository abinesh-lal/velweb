<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CetegoryHead Entity
 *
 * @property int $Cetegory_IdNo
 * @property string $Cetegory_Name
 * @property string $Sur_Name
 */
class CetegoryHead extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'Cetegory_IdNo' => false
    ];
}
