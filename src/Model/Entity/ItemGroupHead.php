<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ItemGroupHead Entity
 *
 * @property int $ItemGroup_IdNo
 * @property string $ItemGroup_Name
 * @property string $Sur_Name
 * @property string $Commodity_Code
 * @property string $Item_HSN_Code
 * @property float $Item_GST_Percentage
 * @property int $Cetegory_IdNo
 */
class ItemGroupHead extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'ItemGroup_IdNo' => false
    ];
}
