<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SalesmanHead Entity
 *
 * @property int $Salesman_Idno
 * @property string $Salesman_Name
 * @property string $Sur_Name
 * @property string $SalesMan_EmailID2
 * @property string $SalesMan_EmailID
 * @property float $Collection_Amount
 * @property float $Total_Amount
 * @property float $Total_Quantity
 * @property int $Month_Idno
 */
class SalesmanHead extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'Salesman_Idno' => false
    ];
}
