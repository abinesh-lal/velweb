<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ItemPortfolioHead Entity
 *
 * @property int $ItemPortfolio_IdNo
 * @property string $ItemPortfolio_FolderNameList
 * @property int $Item_idNo
 */
class ItemPortfolioHead extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'ItemPortfolio_IdNo' => false
    ];
}
