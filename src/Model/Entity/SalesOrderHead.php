<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SalesOrderHead Entity
 *
 * @property string $Sales_Order_Code
 * @property int $Company_IdNo
 * @property string $Sales_Order_No
 * @property float $for_OrderBy
 * @property \Cake\I18n\Time $Sales_Order_Date
 * @property string $Payment_Method
 * @property int $Ledger_IdNo
 * @property string $Cash_PartyName
 * @property string $Party_PhoneNo
 * @property int $SalesAc_IdNo
 * @property string $Tax_Type
 * @property int $TaxAc_IdNo
 * @property string $Delivery_Address1
 * @property string $Delivery_Address2
 * @property string $Delivery_Address3
 * @property string $Vehicle_No
 * @property string $Narration
 * @property float $Total_Qty
 * @property int $Total_Bags
 * @property float $SubTotal_Amount
 * @property float $Total_DiscountAmount
 * @property float $Total_TaxAmount
 * @property float $Gross_Amount
 * @property float $CashDiscount_Perc
 * @property float $CashDiscount_Amount
 * @property float $Assessable_Value
 * @property float $Tax_Perc
 * @property float $Tax_Amount
 * @property float $Freight_Amount
 * @property float $AddLess_Amount
 * @property float $Round_Off
 * @property float $Net_Amount
 * @property string $Document_Through
 * @property string $Despatch_To
 * @property string $Lr_No
 * @property string $Lr_Date
 * @property string $Booked_By
 * @property int $Transport_IdNo
 * @property float $Freight_ToPay_Amount
 * @property string $Dc_No
 * @property string $Dc_Date
 * @property string $Order_No
 * @property string $Order_Date
 * @property int $Against_CForm_Status
 * @property string $Entry_Type
 * @property string $Payment_Terms
 * @property int $OnAc_IdNo
 * @property float $Extra_Charges
 * @property float $Total_Extra_Copies
 * @property float $Sub_Total_Copies
 * @property string $Party_Name
 * @property float $Weight
 * @property int $Sales_OrderAc_IdNo
 * @property int $Order_Close
 * @property string $Sales_Order_Selection_Code
 * @property string $Quotation_No
 * @property string $Quotation_Date
 * @property int $Salesman_IdNo
 * @property string $Removal_Date
 * @property string $Date_Time_Of_Supply
 * @property float $Total_DiscountAmount_item
 * @property string $Entry_VAT_GST_Type
 * @property string $Entry_GST_Tax_Type
 * @property float $CGst_Amount
 * @property float $SGst_Amount
 * @property float $IGst_Amount
 * @property int $DeliveryTo_IdNo
 */
class SalesOrderHead extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'Sales_Order_Code' => false
    ];
}
