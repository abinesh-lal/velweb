<?php
namespace App\Controller;

use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Network\Exception\NotFoundException;
use App\Controller\ServiceController;
use Cake\Utility\Security;

class ApiController extends AppController
{
	private $service;

    public function initialize() {
        //initializing the API and fixing the response type as json
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->viewBuilder()->className('Json');

        $this->response->header('Access-Control-Allow-Origin', '*');

        //initializing object for service controller
        $this->service = new ServiceController();
    }

    /**
    * Login
    *
    * This function will login the user
    *
    */
    public function login() {
        // API should works only for POST requests
        if ($this->request->is('post')) {
            $loginData = false;

            // read the data from post
            $username = $this->request->data['username'];
            $password = $this->request->data['password'];

            // pass the data to the service controller
            $salesmanDetails = $this->service->_login($username, $password);

            $response = [
                'status' => ($salesmanDetails) ? 'success' : 'failure',
                'message' => ($salesmanDetails) ? 'Login Successfully' : 'Invalid Credentials',
                '_serialize' => ['status', 'message', 'user_id']
            ];

            if($salesmanDetails) {
                $response['user_id'] = $salesmanDetails['Salesman_Idno'];
            }

            // return the status
            $this->set($response);
        } else {
            throw new NotFoundException();
        }
    }

    /**
     * Dealers List
     * 
     * This function will list all dealers by sales man id
     */
    public function dealers($user_id = null, $keyword = null) {
        // API should works only for POST requests
        if ($this->request->is('get') && $user_id != null && $keyword != null) {

            // pass the data to the service controller
            $dealersList = $this->service->_getDealers($user_id, $keyword);
            
            $dealers = array();

            foreach($dealersList as $dealer) {
                $dealers[] = array(
                    'id' => $dealer['Ledger_IdNo'],
                    'name' => $dealer['Ledger_MainName'],
                    'phone' => $dealer['Ledger_PhoneNo'],
                    'area' => $dealer['AreaHead']['Area_Name']
                );
            }

            $response = [
                'dealers' => $dealers,
                '_serialize' => ['dealers']
            ];

            // return the status
            $this->set($response);
        } else {
            throw new NotFoundException();
        }
    }

    /**
     * Categories list
     * 
     * This function will list all categories
     */
    public function categories($user_id = null, $dealer_id = null) {
        // API should works only for POST requests
        if ($this->request->is('get') && $user_id != null & $dealer_id != null) {

            // pass the data to the service controller
            $categoriesList = $this->service->_getCategories($user_id, $dealer_id);
            
            $categories = array();

            foreach($categoriesList as $category) {
                $categories[] = array(
                    'id' => $category['Cetegory_IdNo'],
                    'name' => $category['Cetegory_Name']
                );
            }

            $response = [
                'categories' => $categories,
                '_serialize' => ['categories']
            ];

            // return the status
            $this->set($response);
        } else {
            throw new NotFoundException();
        }
    }

    /**
     * Sub categories list
     * 
     * This function will list all sub categories
     */
    public function subCategories($user_id = null, $dealer_id = null, $category_id = null) {
        // API should works only for POST requests
        if ($this->request->is('get') && $user_id != null & $dealer_id != null && $category_id != null) {

            // pass the data to the service controller
            $subCategoriesList = $this->service->_getSubCategories($user_id, $dealer_id, $category_id);
            
            $subCategories = array();

            foreach($subCategoriesList as $subCategory) {
                $subCategories[] = array(
                    'id' => $subCategory['ItemGroup_IdNo'],
                    'name' => $subCategory['ItemGroup_Name']
                );
            }

            $response = [
                'subCategories' => $subCategories,
                '_serialize' => ['subCategories']
            ];

            // return the status
            $this->set($response);
        } else {
            throw new NotFoundException();
        }
    }

    /**
     * Listing all items in stock
     * 
     * This function will list all items in stock
     */
    public function itemsInStock($user_id = null, $dealer_id = null, $category_id = null, $sub_category_id = null) {
        // API should works only for POST requests
        if ($this->request->is('get') && $user_id != null & $dealer_id != null && $category_id != null && $sub_category_id != null) {

            // pass the data to the service controller
            $itemList = $this->service->_getItemsInStock($user_id, $dealer_id, $category_id, $sub_category_id);
            
            $items = array();

            foreach($itemList as $item) {

                $schemeDiscountPercentage = ($item['SchemeDetails_Discount_Percentage']) ? $item['SchemeDetails_Discount_Percentage'] : '0.0';
                $dealerDiscountPercentage = $item['LedgerDiscountDetails_Discount_Percentage'];

                $schemeDiscountAmount = ($item['ItemHead_MRP_Rate'] * $schemeDiscountPercentage) / 100;
                $schemeUCP = $item['ItemHead_MRP_Rate'] - $schemeDiscountAmount;
                $dealerDiscountAmount = ($schemeUCP * $dealerDiscountPercentage) / 100;

                $totalDiscount = $schemeDiscountAmount + $dealerDiscountAmount;

                $billableRate = ($item['ItemHead_MRP_Rate'] - $totalDiscount);

                $gstDetails = [
                    'sgst' => [
                        'percentage' => ($item['LedgerHead_State_Idno'] == 1) ? ($item['ItemHead_Gst_Percentage'] / 2) : 0,
                        'amount' => ($item['LedgerHead_State_Idno'] == 1) ? ($billableRate * ($item['ItemHead_Gst_Percentage'] / 2)) / 100 : 0.0
                    ],
                    'cgst' => [
                        'percentage' => ($item['LedgerHead_State_Idno'] == 1) ? ($item['ItemHead_Gst_Percentage'] / 2) : 0,
                        'amount' => ($item['LedgerHead_State_Idno'] == 1) ? ($billableRate * ($item['ItemHead_Gst_Percentage'] / 2)) / 100 : 0.0
                    ],
                    'igst' => [
                        'percentage' => ($item['LedgerHead_State_Idno'] != 1) ? ($item['ItemHead_Gst_Percentage']) : 0,
                        'amount' => ($item['LedgerHead_State_Idno'] != 1) ? ($billableRate * $item['ItemHead_Gst_Percentage']) / 100 : 0.0
                    ]
                ];

                $discountDetails = [
                    'scheme' => [
                        'percentage' => $schemeDiscountPercentage,
                        'amount' => $schemeDiscountAmount
                    ],
                    'dealer' => [
                        'percentage' => $dealerDiscountPercentage,
                        'amount' => $dealerDiscountAmount
                    ]
                ];

                $taxableRate = $billableRate - (($gstDetails['igst']['percentage'] == 0) ? ($gstDetails['sgst']['amount'] + $gstDetails['cgst']['amount']) : $gstDetails['igst']['amount']);

                $items[] = array(
                    'id' => $item['ItemHead_Item_IdNo'],
                    'name' => $item['ItemHead_Item_Name'],
                    'itemCode' => $item['ItemHead_Item_Code'],
                    'description' => $item['ItemHead_Item_Description'],
                    'mrp' => $item['ItemHead_MRP_Rate'],
                    'gst' => $item['ItemHead_Gst_Percentage'],
                    'gstDetails' => $gstDetails,
                    'state' => $item['LedgerHead_State_Idno'],
                    'discountDetails' => $discountDetails,
                    'schemeUCP' => $schemeUCP,
                    'totalDiscount' => $totalDiscount,
                    'billableRate' => $billableRate,
                    'taxableRate' => $taxableRate,
                    'images' => $item['ItemPortfolioHead_ItemPortfolio_FolderNameList'],
                    'stock' => $item['ItemProcessingDetails_Stock']
                );
            }

            $response = [
                'items' => $items,
                '_serialize' => ['items']
            ];

            // return the status
            $this->set($response);
        } else {
            throw new NotFoundException();
        }
    }

    /**
     * Listing all items not in stock
     * 
     * This function will list all items not in stock
     */
    public function itemsNotInStock($user_id = null, $dealer_id = null, $category_id = null, $sub_category_id = null) {
        // API should works only for POST requests
        if ($this->request->is('get') && $user_id != null & $dealer_id != null && $category_id != null && $sub_category_id != null) {

            // pass the data to the service controller
            $itemList = $this->service->_getItemsNotInStock($user_id, $dealer_id, $category_id, $sub_category_id);
            
            $items = array();

            foreach($itemList as $item) {

                $schemeDiscountPercentage = ($item['SchemeDetails_Discount_Percentage']) ? $item['SchemeDetails_Discount_Percentage'] : '0.0';
                $dealerDiscountPercentage = $item['LedgerDiscountDetails_Discount_Percentage'];

                $schemeDiscountAmount = ($item['ItemHead_MRP_Rate'] * $schemeDiscountPercentage) / 100;
                $schemeUCP = $item['ItemHead_MRP_Rate'] - $schemeDiscountAmount;
                $dealerDiscountAmount = ($schemeUCP * $dealerDiscountPercentage) / 100;

                $totalDiscount = $schemeDiscountAmount + $dealerDiscountAmount;

                $billableRate = ($item['ItemHead_MRP_Rate'] - $totalDiscount);

                $gstDetails = [
                    'sgst' => [
                        'percentage' => ($item['LedgerHead_State_Idno'] == 1) ? ($item['ItemHead_Gst_Percentage'] / 2) : 0,
                        'amount' => ($item['LedgerHead_State_Idno'] == 1) ? ($billableRate * ($item['ItemHead_Gst_Percentage'] / 2)) / 100 : 0.0
                    ],
                    'cgst' => [
                        'percentage' => ($item['LedgerHead_State_Idno'] == 1) ? ($item['ItemHead_Gst_Percentage'] / 2) : 0,
                        'amount' => ($item['LedgerHead_State_Idno'] == 1) ? ($billableRate * ($item['ItemHead_Gst_Percentage'] / 2)) / 100 : 0.0
                    ],
                    'igst' => [
                        'percentage' => ($item['LedgerHead_State_Idno'] != 1) ? ($item['ItemHead_Gst_Percentage']) : 0,
                        'amount' => ($item['LedgerHead_State_Idno'] != 1) ? ($billableRate * $item['ItemHead_Gst_Percentage']) / 100 : 0.0
                    ]
                ];

                $discountDetails = [
                    'scheme' => [
                        'percentage' => $schemeDiscountPercentage,
                        'amount' => $schemeDiscountAmount
                    ],
                    'dealer' => [
                        'percentage' => $dealerDiscountPercentage,
                        'amount' => $dealerDiscountAmount
                    ]
                ];

                $taxableRate = $billableRate - (($gstDetails['igst']['percentage'] == 0) ? ($gstDetails['sgst']['amount'] + $gstDetails['cgst']['amount']) : $gstDetails['igst']['amount']);

                $items[] = array(
                    'id' => $item['ItemHead_Item_IdNo'],
                    'name' => $item['ItemHead_Item_Name'],
                    'itemCode' => $item['ItemHead_Item_Code'],
                    'description' => $item['ItemHead_Item_Description'],
                    'mrp' => $item['ItemHead_MRP_Rate'],
                    'gst' => $item['ItemHead_Gst_Percentage'],
                    'gstDetails' => $gstDetails,
                    'state' => $item['LedgerHead_State_Idno'],
                    'discountDetails' => $discountDetails,
                    'schemeUCP' => $schemeUCP,
                    'totalDiscount' => $totalDiscount,
                    'billableRate' => $billableRate,
                    'taxableRate' => $taxableRate,
                    'images' => $item['ItemPortfolioHead_ItemPortfolio_FolderNameList'],
                    'stock' => $item['ItemProcessingDetails_Stock']
                );
            }

            $response = [
                'items' => $items,
                '_serialize' => ['items']
            ];

            // return the status
            $this->set($response);
        } else {
            throw new NotFoundException();
        }
    }

    /**
     * Place Sale Order
     * 
     * This function will place a sale order
     */
    public function checkout($user_id = null, $dealer_id = null) {
        // API should works only for POST requests
        if ($this->request->is('post') && $user_id != null & $dealer_id != null) {

            // pass the data to the service controller
            $itemList = $this->service->_checkout($user_id, $dealer_id, $this->request->data);
            

            $response = [
                'items' => $itemList,
                '_serialize' => ['items']
            ];

            // return the status
            $this->set($response);
        } else {
            throw new NotFoundException();
        }
    }

    /**
     * Listing Dealer History
     */

     public function dealerHistory($user_id = null, $dealer_id = null) {
        // API should works only for POST requests
        if ($this->request->is('get') && $user_id != null & $dealer_id != null) {

            // pass the data to the service controller
            $dealerHistory = $this->service->_dealerHistory($user_id, $dealer_id);
            

            $response = [
                'history' => $dealerHistory,
                '_serialize' => ['history']
            ];

            // return the status
            $this->set($response);
        } else {
            throw new NotFoundException();
        }
     }
}