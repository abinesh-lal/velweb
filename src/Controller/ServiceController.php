<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Exception\Exception;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;
use Cake\I18n\Time;

/**
 * Service Controller
 *
 * This controller will treat as a Business layer
 */
class ServiceController extends AppController
{
    private $connectionManager = null;

    public function initialize() {
        parent::initialize();

        $this->connectionManager = ConnectionManager::get('default');
    }

    /**
     * Login
     * 
     * This function will validate sales person
     * @param username
     * @param password
     */
    public function _login($username, $password) {
        
        $salesmanDetails = array();
        try {
            $this->connectionManager->begin();

            $salesmanHeadTable = TableRegistry::get('SalesmanHead');
            $salesmanLoginHeadTable = TableRegistry::get('SalesmanLoginHead');

            $salesmanDetails = $salesmanHeadTable->find('all', [
                'fields' => [
                    'SalesmanHead.Salesman_Idno'
                ],
                'conditions' => [
                    "SalesmanHead.SalesMan_EmailID" => $username,
                    "SalesmanLoginHead.Password" => $password
                ],
                'join' => array(
                    'SalesmanLoginHead' => [
                        'table' => 'Salesman_Login_Head',
                        'type' => 'INNER',
                        'conditions' => 'SalesmanLoginHead.Salesman_Idno = SalesmanHead.Salesman_Idno'
                    ]
                )
            ])->first();

        } catch (\Exception $ex) {
            $this->connectionManager->rollback();
            Log::write(LOG_ERR, '================================');
            Log::write(LOG_ERR, 'Error occured while logging in the user - '.$username);
            Log::write(LOG_ERR, 'Message: '.$ex->getMessage());
            Log::write(LOG_ERR, 'Raw Message:');
            Log::write(LOG_ERR, $ex);
        }

        return $salesmanDetails;
    }

    /**
     * 
     * This function will list all dealers by sales man id
     * 
     * @param user_id
     */
    public function _getDealers($user_id, $keyword) {

        $dealerList = array();
        try {
            $this->connectionManager->begin();

            $ledgerHeadTable = TableRegistry::get('LedgerHead');

            $dealerList = $ledgerHeadTable->find('all', [
                'fields' => [
                    'LedgerHead.Ledger_IdNo',
                    'LedgerHead.Ledger_MainName',
                    'LedgerHead.Ledger_PhoneNo',
                    'AreaHead.Area_Name'
                ],
                'conditions' => [
                    "LedgerHead.SalesMan_Idno" => $user_id,
                    "LOWER(LedgerHead.Ledger_MainName) LIKE" => strtolower($keyword) . "%"
                ],
                'join' => array(
                    'AreaHead' => [
                        'table' => 'Area_Head',
                        'type' => 'INNER',
                        'conditions' => 'AreaHead.Area_IdNo = LedgerHead.Area_IdNo'
                    ]
                )
            ])->toArray();

        } catch (\Exception $ex) {
            $this->connectionManager->rollback();
            Log::write(LOG_ERR, '================================');
            Log::write(LOG_ERR, 'Error occured while listing all dealers');
            Log::write(LOG_ERR, 'Message: '.$ex->getMessage());
            Log::write(LOG_ERR, 'Raw Message:');
            Log::write(LOG_ERR, $ex);
        }

        return $dealerList;
    }

    /**
     * 
     * This function will list all categories by ledger id
     * 
     * @param user_id
     * @param dealer_id
     */
    public function _getCategories($user_id, $dealer_id) {

        $categoriesList = array();
        try {
            $this->connectionManager->begin();

            $categoryHeadTable = TableRegistry::get('CetegoryHead');

            $categoriesList = $categoryHeadTable->find('all', [
                'fields' => [
                    'Cetegory_IdNo' => 'DISTINCT CetegoryHead.Cetegory_IdNo',
                    'Cetegory_Name' => 'CetegoryHead.Cetegory_Name'
                ],
                'conditions' => [
                    'CetegoryHead.Cetegory_IdNo !=' => '0',
                    'LedgerDiscountDetails.Ledger_IdNo' => $dealer_id
                ],
                'join' => array(
                    'ItemGroupHead' => [
                        'table' => 'ItemGroup_Head',
                        'type' => 'INNER',
                        'conditions' => 'ItemGroupHead.Cetegory_IdNo = CetegoryHead.Cetegory_IdNo'
                    ],
                    'LedgerDiscountDetails' => [
                        'table' => 'Ledger_DiscountDetails',
                        'type' => 'INNER',
                        'conditions' => 'LedgerDiscountDetails.ItemGroup_IdNo = ItemGroupHead.ItemGroup_IdNo'
                    ]
                )
            ])->toArray();

        } catch (\Exception $ex) {
            $this->connectionManager->rollback();
            Log::write(LOG_ERR, '================================');
            Log::write(LOG_ERR, 'Error occured while listing categories');
            Log::write(LOG_ERR, 'Message: '.$ex->getMessage());
            Log::write(LOG_ERR, 'Raw Message:');
            Log::write(LOG_ERR, $ex);
        }

        return $categoriesList;
    }

    /**
     * 
     * This function will list all sub categories by ledger id
     * 
     * @param user_id
     * @param dealer_id
     * @param category_id
     */
    public function _getSubCategories($user_id, $dealer_id, $category_id) {

        $subCategoriesList = array();
        try {
            $this->connectionManager->begin();

            $itemGroupHeadTable = TableRegistry::get('ItemGroupHead');

            $subCategoriesList = $itemGroupHeadTable->find('all', [
                'fields' => [
                    'ItemGroupHead.ItemGroup_IdNo',
                    'ItemGroupHead.ItemGroup_Name'
                ],
                'conditions' => [
                    'ItemGroupHead.ItemGroup_IdNo !=' => '0' ,
                    'ItemGroupHead.Cetegory_IdNo' => $category_id,
                    'LedgerDiscountDetails.Ledger_IdNo' => $dealer_id
                ],
                'join' => array(
                    'LedgerDiscountDetails' => [
                        'table' => 'Ledger_DiscountDetails',
                        'type' => 'INNER',
                        'conditions' => 'LedgerDiscountDetails.ItemGroup_IdNo = ItemGroupHead.ItemGroup_IdNo'
                    ]
                )
            ])->toArray();

        } catch (\Exception $ex) {
            $this->connectionManager->rollback();
            Log::write(LOG_ERR, '================================');
            Log::write(LOG_ERR, 'Error occured while listing sub categories');
            Log::write(LOG_ERR, 'Message: '.$ex->getMessage());
            Log::write(LOG_ERR, 'Raw Message:');
            Log::write(LOG_ERR, $ex);
        }

        return $subCategoriesList;
    }

    /**
     * 
     * This function will list all items in stock by item group id
     * 
     * @param user_id
     * @param dealer_id
     * @param category_id
     * @param sub_category_id
     */
    public function _getItemsInStock($user_id, $dealer_id, $category_id, $sub_category_id) {

        $itemList = array();
        try {
            $this->connectionManager->begin();

            $itemHeadTable = TableRegistry::get('ItemHead');

            $itemList = $this->connectionManager->execute("EXEC dbo.ListItemsInStock @ItemGroupId = $sub_category_id, @LedgerId = $dealer_id")->fetchAll('assoc');

            // $itemList = $itemHeadTable->find('all', [
            //     'fields' => [
            //         'ItemHead.Item_IdNo',
            //         'ItemHead.Item_Name',
            //         'ItemHead.Item_Code',
            //         'ItemHead.Item_Description',
            //         'ItemHead.MRP_Rate',
            //         'LedgerDiscountDetails.Discount_Percentage',
            //         'SchemeDetails.Discount_Percentage',
            //         'ItemHead.Gst_Percentage',
            //         'LedgerHead.State_Idno',
            //         'ItemPortfolioHead.ItemPortfolio_FolderNameList'
            //     ],
            //     'conditions' => [
            //         'ItemHead.Item_IdNo !=' => '0' ,
            //         'ItemHead.ItemGroup_IdNo' => $sub_category_id,
            //         'LedgerDiscountDetails.Ledger_IdNo' => $dealer_id
            //     ],
            //     'join' => array(
            //         'LedgerDiscountDetails' => [
            //             'table' => 'Ledger_DiscountDetails',
            //             'type' => 'INNER',
            //             'conditions' => 'LedgerDiscountDetails.ItemGroup_IdNo = ItemHead.ItemGroup_IdNo'
            //         ],
            //         'LedgerHead' => [
            //             'table' => 'Ledger_Head',
            //             'type' => 'INNER',
            //             'conditions' => 'LedgerHead.Ledger_IdNo = LedgerDiscountDetails.Ledger_IdNo'
            //         ],
            //         'SchemeDetails' => [
            //             'table' => 'Scheme_Details',
            //             'type' => 'LEFT',
            //             'conditions' => ['SchemeDetails.Item_IdNo = ItemHead.Item_IdNo', 'SchemeDetails.Secondary_EndDate >= DATEADD(day, DATEDIFF(day, 0, GETDATE()), 0)']
            //         ],
            //         'ItemPortfolioHead' => [
            //             'table' => 'Item_Portfolio_Head',
            //             'type' => 'LEFT',
            //             'conditions' => ['ItemPortfolioHead.Item_IdNo = ItemHead.Item_IdNo']
            //         ]
            //     )
            // ])->toArray();

        } catch (\Exception $ex) {
            $this->connectionManager->rollback();
            Log::write(LOG_ERR, '================================');
            Log::write(LOG_ERR, 'Error occured while listing items in stock');
            Log::write(LOG_ERR, 'Message: '.$ex->getMessage());
            Log::write(LOG_ERR, 'Raw Message:');
            Log::write(LOG_ERR, $ex);
        }

        return $itemList;
    }

    /**
     * 
     * This function will list all items not in stock by item group id
     * 
     * @param user_id
     * @param dealer_id
     * @param category_id
     * @param sub_category_id
     */
    public function _getItemsNotInStock($user_id, $dealer_id, $category_id, $sub_category_id) {

        $itemList = array();
        try {
            $this->connectionManager->begin();

            $itemHeadTable = TableRegistry::get('ItemHead');

            $itemList = $this->connectionManager->execute("EXEC dbo.ListItemsNotInStock @ItemGroupId = $sub_category_id, @LedgerId = $dealer_id")->fetchAll('assoc');

            // $itemList = $itemHeadTable->find('all', [
            //     'fields' => [
            //         'ItemHead.Item_IdNo',
            //         'ItemHead.Item_Name',
            //         'ItemHead.Item_Code',
            //         'ItemHead.Item_Description',
            //         'ItemHead.MRP_Rate',
            //         'LedgerDiscountDetails.Discount_Percentage',
            //         'SchemeDetails.Discount_Percentage',
            //         'ItemHead.Gst_Percentage',
            //         'LedgerHead.State_Idno',
            //         'ItemPortfolioHead.ItemPortfolio_FolderNameList'
            //     ],
            //     'conditions' => [
            //         'ItemHead.Item_IdNo !=' => '0' ,
            //         'ItemHead.ItemGroup_IdNo' => $sub_category_id,
            //         'LedgerDiscountDetails.Ledger_IdNo' => $dealer_id
            //     ],
            //     'join' => array(
            //         'LedgerDiscountDetails' => [
            //             'table' => 'Ledger_DiscountDetails',
            //             'type' => 'INNER',
            //             'conditions' => 'LedgerDiscountDetails.ItemGroup_IdNo = ItemHead.ItemGroup_IdNo'
            //         ],
            //         'LedgerHead' => [
            //             'table' => 'Ledger_Head',
            //             'type' => 'INNER',
            //             'conditions' => 'LedgerHead.Ledger_IdNo = LedgerDiscountDetails.Ledger_IdNo'
            //         ],
            //         'SchemeDetails' => [
            //             'table' => 'Scheme_Details',
            //             'type' => 'LEFT',
            //             'conditions' => ['SchemeDetails.Item_IdNo = ItemHead.Item_IdNo', 'SchemeDetails.Secondary_EndDate >= DATEADD(day, DATEDIFF(day, 0, GETDATE()), 0)']
            //         ],
            //         'ItemPortfolioHead' => [
            //             'table' => 'Item_Portfolio_Head',
            //             'type' => 'LEFT',
            //             'conditions' => ['ItemPortfolioHead.Item_IdNo = ItemHead.Item_IdNo']
            //         ]
            //     )
            // ])->toArray();

        } catch (\Exception $ex) {
            $this->connectionManager->rollback();
            Log::write(LOG_ERR, '================================');
            Log::write(LOG_ERR, 'Error occured while listing items not in stock');
            Log::write(LOG_ERR, 'Message: '.$ex->getMessage());
            Log::write(LOG_ERR, 'Raw Message:');
            Log::write(LOG_ERR, $ex);
        }

        return $itemList;
    }

    /**
     * This function will place the Sales order
     * 
     * @param user_id
     * @param dealer_id
     * @param category_id
     * @param sub_category_id
     */
    public function _checkout($user_id, $dealer_id, $salesOrder) {
        $status = array();

        // try {
            // $this->connectionManager->begin();

            $itemHeadTable = TableRegistry::get('ItemHead');
            $salesOrderDetailsTable = TableRegistry::get('SalesOrderDetails');
            $salesOrderGstTaxDetailsTable = TableRegistry::get('SalesOrderGstTaxDetails');
            $salesOrderHeadTable = TableRegistry::get('SalesOrderHead');

            $financialYear = ( date('m') > 3 ) ? (date('y') . '-' . (date('y') + 1)) : ((date('y') - 1) . '-' . date('y'));

            $maxSalesOrderNo = $salesOrderDetailsTable->find('all', [
                'fields' => [
                    'salesOrderNo' => 'max(for_OrderBy)'
                ],
                'conditions' => [
                    'Sales_Order_Code LIKE ' => '%/'.$financialYear
                ]
            ])->first();

            $maxSalesOrderDetailsSlNo = $salesOrderDetailsTable->find('all', [
                'fields' => [
                    'salesOrderDetailsSlNo' => 'max(Sales_Order_Detail_SlNo)'
                ]
            ])->first();

            $status['year'] = $financialYear;
            $status['max'] = $maxSalesOrderNo['salesOrderNo'];
            $status['items'] = $salesOrder['items'];

            $salesOrderNo = $maxSalesOrderNo['salesOrderNo']+1;
            $salesOrderCode = "GSAOD-1-".$salesOrderNo."/".$financialYear;
            $salesOrderDetailsSlNo = $maxSalesOrderDetailsSlNo['salesOrderDetailsSlNo'];

            $gstDetails = [];
            $salesDetails = [
                'SubTotalAmount' => 0,
                'TotalDiscountAmount' => 0,
                'NetAmount' => 0,
                'CgstAmount' => 0,
                'SgstAmount' => 0,
                'IgstAmount' => 0
            ];
            foreach ($salesOrder['items'] as $itemIndex => $item) {
                $itemObj = json_decode($item, true);
                $category_id = $itemObj['category'];
                $sub_category_id = $itemObj['subCategory'];

                $itemHead = $itemHeadTable->find('all', [
                    'fields' => [
                        'ItemHead.Unit_IdNo',
                        'ItemHead.MRP_Rate',
                        'ItemHead.Gst_Percentage',
                        'ItemHead.MRP_Rate',
                        'ItemHead.Item_Description',
                        'ItemGroupHead.Item_HSN_Code'
                    ],
                    'conditions' => [
                        'Item_IdNo' => $itemObj['itemId']
                    ],
                    'join' => array(
                        'ItemGroupHead' => [
                            'table' => 'ItemGroup_Head',
                            'type' => 'INNER',
                            'conditions' => 'ItemGroupHead.ItemGroup_IdNo = ItemHead.ItemGroup_IdNo'
                        ]
                    )
                ])->first();


                $totalAmount = ($itemHead['MRP_Rate'] * $itemObj['quantity']);
                $schemeDiscountAmount = $totalAmount * $itemObj['discount']['scheme'] / 100;
                $dealerDiscountAmount = $schemeDiscountAmount * $itemObj['discount']['dealer'] / 100;

                $schemeUCP = $totalAmount - $schemeDiscountAmount;
                $totalDiscount = $schemeDiscountAmount + $dealerDiscountAmount;

                $netAmount = $totalAmount - $totalDiscount;

                $cgstPercentage = (($itemObj['state'] == 1) ? ($itemObj['gst'] / 2) : 0);
                $cgstAmount = $totalAmount * (($itemObj['state'] == 1) ? ($itemObj['gst'] / 2) : 0) / 100;
                $sgstPercentage = (($itemObj['state'] == 1) ? ($itemObj['gst'] / 2) : 0);
                $sgstAmount = $totalAmount * (($itemObj['state'] == 1) ? ($itemObj['gst'] / 2) : 0) / 100;
                $igstPercentage = (($itemObj['state'] != 1) ? $itemObj['gst'] : 0);
                $igstAmount = $totalAmount * (($itemObj['state'] != 1) ? $itemObj['gst'] : 0) / 100;
                $assessableAmount = $netAmount - ($cgstAmount + $sgstAmount + $igstAmount);

                $salesOrderDetails = [
                    "Sales_Order_Code" => $salesOrderCode,
                    "Company_IdNo" => "1",
                    "Sales_Order_No" => $salesOrderNo,
                    "for_OrderBy" => $salesOrderNo,
                    "Sales_Order_Date" => date("Y-m-d 00:00:00"),
                    "Ledger_IdNo" => $dealer_id,
                    "SL_No" => ($itemIndex+1),
                    "Item_IdNo" => $itemObj['itemId'],
                    "ItemGroup_IdNo" => $sub_category_id,
                    "Unit_IdNo" => $itemHead['Unit_IdNo'],
                    "Noof_Items" => $itemObj['quantity'],
                    "Rate" => $itemHead['MRP_Rate'],
                    "Tax_Rate" => "",
                    "Amount" => $totalAmount,
                    "Tax_Perc" => ($itemHead['Gst_Percentage']),
                    "Total_Amount" => $totalAmount,
                    "Noof_Items_Return" => $itemObj['quantity'],
                    "Sales_Order_Detail_SlNo" => ($salesOrderDetailsSlNo+1),
                    "item_Description" => $itemHead['Item_Description'],
                    "Scheme_Disc_Percentage" => $itemObj['discount']['scheme'],
                    "Scheme_Discount" => $schemeDiscountAmount,
                    "Trade_Disc_Percentage" => $itemObj['discount']['dealer'],
                    "Trade_Discount" => $dealerDiscountAmount,
                    "Cgst_Percentage" => $cgstPercentage,
                    "Cgst_Amount" => $cgstAmount,
                    "Sgst_Percentage" => $sgstPercentage,
                    "Sgst_Amount" => $sgstAmount,
                    "Igst_Percentage" => $igstPercentage,
                    "Igst_Amount" => $igstAmount,
                    "Net_Amount" => $netAmount,
                    "Total_Rate" => $assessableAmount,
                    "Scheme_UCP" => $schemeUCP,
                    "Discount_Total" => $totalDiscount,
                    "HSN_Code" => $itemHead['ItemGroupHead']['Item_HSN_Code'],
                    "Assessable_Value" => $assessableAmount
                ];

                $salesOrderDetailsEntity = $salesOrderDetailsTable->newEntity($salesOrderDetails);

                var_dump($salesOrderDetailsEntity);

                $status['salesOrderDetails'] = $salesOrderDetailsTable->save($salesOrderDetailsEntity);
                // if (!$salesOrderDetailsTable->save($salesOrderDetailsEntity))
                    // throw new Exception("Error occured while saving entry in salesOrderDetails", 1);

                if(!isset($gstDetails[$itemHead['ItemGroupHead']['Item_HSN_Code']])) {
                    $gstDetails[$itemHead['ItemGroupHead']['Item_HSN_Code']] = [
                        'TaxableAmount' => 0,
                        'CgstPercentage' => 0,
                        'SgstPercentage' => 0,
                        'IgstPercentage' => 0
                    ];
                }

                $gstDetails[$itemHead['ItemGroupHead']['Item_HSN_Code']]['TaxableAmount'] += $assessableAmount;
                $gstDetails[$itemHead['ItemGroupHead']['Item_HSN_Code']]['CgstPercentage'] += $cgstPercentage;
                $gstDetails[$itemHead['ItemGroupHead']['Item_HSN_Code']]['SgstPercentage'] += $sgstPercentage;
                $gstDetails[$itemHead['ItemGroupHead']['Item_HSN_Code']]['IgstPercentage'] += $igstPercentage;

                $salesDetails['SubTotalAmount'] += $assessableAmount;
                $salesDetails['TotalDiscountAmount'] += $totalDiscount;
                $salesDetails['NetAmount'] += $netAmount;
                $salesDetails['CgstAmount'] += $cgstAmount;
                $salesDetails['SgstAmount'] += $sgstAmount;
                $salesDetails['IgstAmount'] += $igstAmount;
            }


            $gstDetailsIndex = 1;
            foreach ($gstDetails as $hsnCode => $gstDetail) {
                $salesOrderGstTaxDetails = [
                    "Sales_Order_Code" => $salesOrderCode,
                    "Company_Idno" => 1,
                    "Sales_Order_No" => $salesOrderNo,
                    "For_OrderBy" => $salesOrderNo,
                    "Sales_Order_Date" => date("Y-m-d"),
                    "Ledger_IdNo" => $dealer_id,
                    "Sl_No" => $gstDetailsIndex++,
                    "HSN_Code" => $hsnCode,
                    "Taxable_Amount" => $gstDetail['TaxableAmount'],
                    "CGST_Percentage" => $gstDetail['CgstPercentage'],
                    "CGST_Amount" => ($gstDetail['TaxableAmount'] * $gstDetail['CgstPercentage']) / 100,
                    "SGST_Percentage" => $gstDetail['SgstPercentage'],
                    "SGST_Amount" => ($gstDetail['TaxableAmount'] * $gstDetail['SgstPercentage']) / 100,
                    "IGST_Percentage" => $gstDetail['IgstPercentage'],
                    "IGST_Amount" => ($gstDetail['TaxableAmount'] * $gstDetail['IgstPercentage']) / 100
                ];

                $salesOrderGstTaxDetailsEntity = $salesOrderGstTaxDetailsTable->newEntity($salesOrderGstTaxDetails);

                $status['salesOrderGstTaxDetails'] = $salesOrderGstTaxDetailsTable->save($salesOrderGstTaxDetailsEntity);
                // if (!$salesOrderGstTaxDetailsTable->save($salesOrderGstTaxDetailsEntity))
                //     throw new Exception("Error occured while saving entry in salesOrderGstTaxDetails", 1);
            }

            $salesOrderHead = [
                "Sales_Order_Code" => $salesOrderCode,
                "Company_IdNo" => 1,
                "Sales_Order_No" => $salesOrderNo,
                "for_OrderBy" => $salesOrderNo,
                "Sales_Order_Date" => date("Y-m-d"),
                "Ledger_IdNo" => $dealer_id,
                "Tax_Type" => "GST",
                "Total_Qty" => count($salesOrder['items']),
                "SubTotal_Amount" => $salesDetails['SubTotalAmount'],
                "Total_DiscountAmount" => "",
                "Total_TaxAmount" => "",
                "Gross_Amount" => $salesDetails['SubTotalAmount'],
                "CashDiscount_Perc" => "",
                "CashDiscount_Amount" => "",
                "Assessable_Value" => $salesDetails['SubTotalAmount'],
                "Net_Amount" => $salesDetails['NetAmount'],
                "Document_Through" => "",
                "Salesman_IdNo" => $user_id,
                "Total_DiscountAmount_item" => $salesDetails['TotalDiscountAmount'],
                "Entry_VAT_GST_Type" => "GST",
                "Entry_GST_Tax_Type" => "GST",
                "CGst_Amount" => $salesDetails['CgstAmount'],
                "SGst_Amount" => $salesDetails['SgstAmount'],
                "IGst_Amount" => $salesDetails['IgstAmount'],
            ];

            $salesOrderHeadEntity = $salesOrderHeadTable->newEntity($salesOrderHead);

            $status['salesOrderHead'] = $salesOrderHeadTable->save($salesOrderHeadEntity);
            // if (!$salesOrderHeadTable->save($salesOrderHeadEntity))
            //     throw new Exception("Error occured while saving entry in salesOrderHead", 1);
        // } catch (\Exception $ex) {
        //     $this->connectionManager->rollback();
        //     Log::write(LOG_ERR, '================================');
        //     Log::write(LOG_ERR, 'Error occured while placing sales order (Checkout)');
        //     Log::write(LOG_ERR, 'Message: '.$ex->getMessage());
        //     Log::write(LOG_ERR, 'Raw Message:');
        //     Log::write(LOG_ERR, $ex);
        // }

        return $status;
    }

    /**
     * Dealer History list
     */

    public function _dealerHistory($user_id, $dealer_id) {
        $historyList = array();
        try {
            $this->connectionManager->begin();

            $itemHeadTable = TableRegistry::get('ItemHead');

            $delearSalesOrderList = $this->connectionManager->execute("EXEC dbo.ListDealerHistory @LedgerId = $dealer_id")->fetchAll('assoc');

            foreach ($delearSalesOrderList as $history) {
                if(!isset($historyList[$history['Sales_No']])) {
                    $historyList[$history['Sales_No']]['Sales_No'] = $history['Sales_No'];
                    $historyList[$history['Sales_No']]['Order_Date'] = $history['Order_Date'];
                    $historyList[$history['Sales_No']]['Assessable_Value'] = (int) $history['Assessable_Value'];
                }

                $historyList[$history['Sales_No']]['Items'][] = array(
                    "Item_Code" => $history['Item_Code'],
                    "Noof_Items" => (int) $history['Noof_Items'],
                    "Scheme_Discount" => (int) $history['Scheme_Discount'],
                    "Scheme_Disc_Percentage" => $history['Scheme_Disc_Percentage'],
                    "Scheme_UCP" => (int) $history['Scheme_UCP'],
                    "Trade_Discount" => (int) $history['Trade_Discount'],
                    "Trade_Disc_Percentage" => $history['Trade_Disc_Percentage'],
                    "Rate" => (int) $history['Rate'],
                    "Assessable_Value" => (int) $history['Item_Assessable_Value']
                );

            }
        } catch (\Exception $ex) {
            $this->connectionManager->rollback();
            Log::write(LOG_ERR, '================================');
            Log::write(LOG_ERR, 'Error occured while listing dealer history');
            Log::write(LOG_ERR, 'Message: '.$ex->getMessage());
            Log::write(LOG_ERR, 'Raw Message:');
            Log::write(LOG_ERR, $ex);
        }
        return $historyList;
    }

}