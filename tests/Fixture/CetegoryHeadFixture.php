<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CetegoryHeadFixture
 *
 */
class CetegoryHeadFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'cetegory_head';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'Cetegory_IdNo' => ['type' => 'integer', 'length' => '10', 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Cetegory_Name' => ['type' => 'string', 'length' => '50', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Sur_Name' => ['type' => 'string', 'length' => '50', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['Cetegory_IdNo'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'Cetegory_IdNo' => 1,
            'Cetegory_Name' => 'Lorem ipsum dolor sit amet',
            'Sur_Name' => 'Lorem ipsum dolor sit amet'
        ],
    ];
}
