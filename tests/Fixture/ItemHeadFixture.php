<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ItemHeadFixture
 *
 */
class ItemHeadFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'item_head';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'Item_IdNo' => ['type' => 'integer', 'length' => '10', 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Item_Name' => ['type' => 'string', 'length' => '100', 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Sur_Name' => ['type' => 'string', 'length' => '100', 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Item_Code' => ['type' => 'string', 'length' => '50', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'ItemGroup_IdNo' => ['type' => 'integer', 'length' => '5', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Unit_IdNo' => ['type' => 'integer', 'length' => '5', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Tax_Percentage' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Sale_TaxRate' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Sales_Rate' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Cost_Rate' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Minimum_Stock' => ['type' => 'decimal', 'length' => '18', 'precision' => '3', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Price_List_IdNo' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Ledger_IdNo' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Item_Name_Tamil' => ['type' => 'string', 'length' => '35', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Item_Description' => ['type' => 'string', 'length' => '500', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Gst_Percentage' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Gst_Rate' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Job_Work_Status' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'MRP_Rate' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Discount_Percentage' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Rack_No' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Sales_Profit_Retail' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Sales_Rate_Retail' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Sales_Profit_Wholesale' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Sales_Rate_Wholesale' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['Item_IdNo'], 'length' => []],
            'IX_Item_Head' => ['type' => 'unique', 'columns' => ['Sur_Name'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'Item_IdNo' => 1,
            'Item_Name' => 'Lorem ipsum dolor sit amet',
            'Sur_Name' => 'Lorem ipsum dolor sit amet',
            'Item_Code' => 'Lorem ipsum dolor sit amet',
            'ItemGroup_IdNo' => 1,
            'Unit_IdNo' => 1,
            'Tax_Percentage' => 1.5,
            'Sale_TaxRate' => 1.5,
            'Sales_Rate' => 1.5,
            'Cost_Rate' => 1.5,
            'Minimum_Stock' => 1.5,
            'Price_List_IdNo' => 1,
            'Ledger_IdNo' => 1,
            'Item_Name_Tamil' => 'Lorem ipsum dolor sit amet',
            'Item_Description' => 'Lorem ipsum dolor sit amet',
            'Gst_Percentage' => 1.5,
            'Gst_Rate' => 1.5,
            'Job_Work_Status' => 1,
            'MRP_Rate' => 1.5,
            'Discount_Percentage' => 1.5,
            'Rack_No' => 'Lorem ipsum dolor sit amet',
            'Sales_Profit_Retail' => 1.5,
            'Sales_Rate_Retail' => 1.5,
            'Sales_Profit_Wholesale' => 1.5,
            'Sales_Rate_Wholesale' => 1.5
        ],
    ];
}
