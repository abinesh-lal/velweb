<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SalesmanLoginHeadFixture
 *
 */
class SalesmanLoginHeadFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'salesman_login_head';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'Salesman_Idno' => ['type' => 'integer', 'length' => '10', 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Password' => ['type' => 'string', 'length' => '100', 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'Salesman_Idno' => 1,
            'Password' => 'Lorem ipsum dolor sit amet'
        ],
    ];
}
