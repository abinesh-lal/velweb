<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ItemGroupHeadFixture
 *
 */
class ItemGroupHeadFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'ItemGroup_Head';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'ItemGroup_IdNo' => ['type' => 'integer', 'length' => '5', 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'ItemGroup_Name' => ['type' => 'string', 'length' => '50', 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Sur_Name' => ['type' => 'string', 'length' => '50', 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Commodity_Code' => ['type' => 'string', 'length' => '20', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Item_HSN_Code' => ['type' => 'string', 'length' => '50', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Item_GST_Percentage' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Cetegory_IdNo' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['ItemGroup_IdNo'], 'length' => []],
            'IX_ItemGroup_Head' => ['type' => 'unique', 'columns' => ['Sur_Name'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'ItemGroup_IdNo' => 1,
            'ItemGroup_Name' => 'Lorem ipsum dolor sit amet',
            'Sur_Name' => 'Lorem ipsum dolor sit amet',
            'Commodity_Code' => 'Lorem ipsum dolor ',
            'Item_HSN_Code' => 'Lorem ipsum dolor sit amet',
            'Item_GST_Percentage' => 1.5,
            'Cetegory_IdNo' => 1
        ],
    ];
}
