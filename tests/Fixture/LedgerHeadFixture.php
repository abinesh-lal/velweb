<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * LedgerHeadFixture
 *
 */
class LedgerHeadFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'ledger_head';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'Ledger_IdNo' => ['type' => 'integer', 'length' => '10', 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Ledger_Name' => ['type' => 'string', 'length' => '100', 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Sur_Name' => ['type' => 'string', 'length' => '100', 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Ledger_MainName' => ['type' => 'string', 'length' => '100', 'null' => false, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Ledger_AlaisName' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Area_IdNo' => ['type' => 'integer', 'length' => '5', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'AccountsGroup_IdNo' => ['type' => 'integer', 'length' => '5', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Parent_Code' => ['type' => 'string', 'length' => '50', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Bill_Type' => ['type' => 'string', 'length' => '50', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Ledger_Address1' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Ledger_Address2' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Ledger_Address3' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Ledger_Address4' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Ledger_PhoneNo' => ['type' => 'string', 'length' => '200', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Ledger_TinNo' => ['type' => 'string', 'length' => '50', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Ledger_CstNo' => ['type' => 'string', 'length' => '50', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Ledger_Type' => ['type' => 'string', 'length' => '50', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Ledger_EmailID' => ['type' => 'string', 'length' => '35', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Price_List_IdNo' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Pan_No' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Rent_Machine' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Free_Copies_Machine' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Rate_Extra_Copy' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Machine_IdNo' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Opening_Reading' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Total_Machine' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'State_Idno' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'LedgerGroup_Idno' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Rate_For_1000' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Minimum_Pcs' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Minimum_Bill_Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Ledger_GSTinNo' => ['type' => 'string', 'length' => '50', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Ledger_PanNo' => ['type' => 'string', 'length' => '50', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Owner_Name' => ['type' => 'string', 'length' => '200', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Birth_Date' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null],
        'Wedding_Date' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null],
        'Agent_idNo' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'SalesMan_Idno' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Ledger_DealerCode' => ['type' => 'string', 'length' => '50', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['Ledger_IdNo'], 'length' => []],
            'IX_Ledger_Head' => ['type' => 'unique', 'columns' => ['Sur_Name'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'Ledger_IdNo' => 1,
            'Ledger_Name' => 'Lorem ipsum dolor sit amet',
            'Sur_Name' => 'Lorem ipsum dolor sit amet',
            'Ledger_MainName' => 'Lorem ipsum dolor sit amet',
            'Ledger_AlaisName' => 'Lorem ipsum dolor sit amet',
            'Area_IdNo' => 1,
            'AccountsGroup_IdNo' => 1,
            'Parent_Code' => 'Lorem ipsum dolor sit amet',
            'Bill_Type' => 'Lorem ipsum dolor sit amet',
            'Ledger_Address1' => 'Lorem ipsum dolor sit amet',
            'Ledger_Address2' => 'Lorem ipsum dolor sit amet',
            'Ledger_Address3' => 'Lorem ipsum dolor sit amet',
            'Ledger_Address4' => 'Lorem ipsum dolor sit amet',
            'Ledger_PhoneNo' => 'Lorem ipsum dolor sit amet',
            'Ledger_TinNo' => 'Lorem ipsum dolor sit amet',
            'Ledger_CstNo' => 'Lorem ipsum dolor sit amet',
            'Ledger_Type' => 'Lorem ipsum dolor sit amet',
            'Ledger_EmailID' => 'Lorem ipsum dolor sit amet',
            'Price_List_IdNo' => 1,
            'Pan_No' => 'Lorem ipsum dolor sit amet',
            'Rent_Machine' => 1.5,
            'Free_Copies_Machine' => 1,
            'Rate_Extra_Copy' => 1.5,
            'Machine_IdNo' => 1,
            'Opening_Reading' => 1,
            'Total_Machine' => 1,
            'State_Idno' => 1,
            'LedgerGroup_Idno' => 1,
            'Rate_For_1000' => 1.5,
            'Minimum_Pcs' => 1.5,
            'Minimum_Bill_Amount' => 1.5,
            'Ledger_GSTinNo' => 'Lorem ipsum dolor sit amet',
            'Ledger_PanNo' => 'Lorem ipsum dolor sit amet',
            'Owner_Name' => 'Lorem ipsum dolor sit amet',
            'Birth_Date' => 1530449355,
            'Wedding_Date' => 1530449355,
            'Agent_idNo' => 1,
            'SalesMan_Idno' => 1,
            'Ledger_DealerCode' => 'Lorem ipsum dolor sit amet'
        ],
    ];
}
