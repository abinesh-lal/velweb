<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SalesOrderGstTaxDetailsFixture
 *
 */
class SalesOrderGstTaxDetailsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'Sales_Order_Code' => ['type' => 'string', 'length' => '50', 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Company_Idno' => ['type' => 'integer', 'length' => '10', 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Sales_Order_No' => ['type' => 'string', 'length' => '50', 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'For_OrderBy' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => false, 'default' => null, 'comment' => null, 'unsigned' => null],
        'Sales_Order_Date' => ['type' => 'timestamp', 'length' => null, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null],
        'Ledger_IdNo' => ['type' => 'integer', 'length' => '10', 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Sl_No' => ['type' => 'integer', 'length' => '10', 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'HSN_Code' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Taxable_Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => null, 'comment' => null, 'unsigned' => null],
        'CGST_Percentage' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => null, 'comment' => null, 'unsigned' => null],
        'CGST_Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => null, 'comment' => null, 'unsigned' => null],
        'SGST_Percentage' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => null, 'comment' => null, 'unsigned' => null],
        'SGST_Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => null, 'comment' => null, 'unsigned' => null],
        'IGST_Percentage' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => null, 'comment' => null, 'unsigned' => null],
        'IGST_Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => null, 'comment' => null, 'unsigned' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['Sales_Order_Code', 'Sl_No'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'Sales_Order_Code' => '05247b49-28d3-4d49-ae50-b5feee78505e',
            'Company_Idno' => 1,
            'Sales_Order_No' => 'Lorem ipsum dolor sit amet',
            'For_OrderBy' => 1.5,
            'Sales_Order_Date' => 1532156792,
            'Ledger_IdNo' => 1,
            'Sl_No' => 1,
            'HSN_Code' => 'Lorem ipsum dolor sit amet',
            'Taxable_Amount' => 1.5,
            'CGST_Percentage' => 1.5,
            'CGST_Amount' => 1.5,
            'SGST_Percentage' => 1.5,
            'SGST_Amount' => 1.5,
            'IGST_Percentage' => 1.5,
            'IGST_Amount' => 1.5
        ],
    ];
}
