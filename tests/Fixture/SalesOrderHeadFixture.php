<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SalesOrderHeadFixture
 *
 */
class SalesOrderHeadFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'sales_order_head';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'Sales_Order_Code' => ['type' => 'string', 'length' => '50', 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Company_IdNo' => ['type' => 'integer', 'length' => '5', 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Sales_Order_No' => ['type' => 'string', 'length' => '50', 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'for_OrderBy' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => false, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Sales_Order_Date' => ['type' => 'timestamp', 'length' => null, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null],
        'Payment_Method' => ['type' => 'string', 'length' => '20', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Ledger_IdNo' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Cash_PartyName' => ['type' => 'string', 'length' => '50', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Party_PhoneNo' => ['type' => 'string', 'length' => '50', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'SalesAc_IdNo' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Tax_Type' => ['type' => 'string', 'length' => '20', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'TaxAc_IdNo' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Delivery_Address1' => ['type' => 'string', 'length' => '50', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Delivery_Address2' => ['type' => 'string', 'length' => '50', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Delivery_Address3' => ['type' => 'string', 'length' => '50', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Vehicle_No' => ['type' => 'string', 'length' => '50', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Narration' => ['type' => 'string', 'length' => '500', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Total_Qty' => ['type' => 'decimal', 'length' => '18', 'precision' => '3', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Total_Bags' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'SubTotal_Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Total_DiscountAmount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Total_TaxAmount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Gross_Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'CashDiscount_Perc' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'CashDiscount_Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Assessable_Value' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Tax_Perc' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Tax_Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Freight_Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'AddLess_Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Round_Off' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Net_Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Document_Through' => ['type' => 'string', 'length' => '35', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Despatch_To' => ['type' => 'string', 'length' => '35', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Lr_No' => ['type' => 'string', 'length' => '35', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Lr_Date' => ['type' => 'string', 'length' => '20', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Booked_By' => ['type' => 'string', 'length' => '35', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Transport_IdNo' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Freight_ToPay_Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Dc_No' => ['type' => 'string', 'length' => '35', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Dc_Date' => ['type' => 'string', 'length' => '35', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Order_No' => ['type' => 'string', 'length' => '50', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Order_Date' => ['type' => 'string', 'length' => '50', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Against_CForm_Status' => ['type' => 'integer', 'length' => '3', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Entry_Type' => ['type' => 'string', 'length' => '20', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Payment_Terms' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'OnAc_IdNo' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Extra_Charges' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Total_Extra_Copies' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Sub_Total_Copies' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Party_Name' => ['type' => 'string', 'length' => '50', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Weight' => ['type' => 'decimal', 'length' => '18', 'precision' => '3', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Sales_OrderAc_IdNo' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Order_Close' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Sales_Order_Selection_Code' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Quotation_No' => ['type' => 'string', 'length' => '30', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Quotation_Date' => ['type' => 'string', 'length' => '30', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Salesman_IdNo' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Removal_Date' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Date_Time_Of_Supply' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Total_DiscountAmount_item' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Entry_VAT_GST_Type' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Entry_GST_Tax_Type' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'CGst_Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'SGst_Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'IGst_Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'DeliveryTo_IdNo' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['Sales_Order_Code'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'Sales_Order_Code' => '6b4c5dc4-1109-4314-8d55-f3f2d79f4168',
            'Company_IdNo' => 1,
            'Sales_Order_No' => 'Lorem ipsum dolor sit amet',
            'for_OrderBy' => 1.5,
            'Sales_Order_Date' => 1532109346,
            'Payment_Method' => 'Lorem ipsum dolor ',
            'Ledger_IdNo' => 1,
            'Cash_PartyName' => 'Lorem ipsum dolor sit amet',
            'Party_PhoneNo' => 'Lorem ipsum dolor sit amet',
            'SalesAc_IdNo' => 1,
            'Tax_Type' => 'Lorem ipsum dolor ',
            'TaxAc_IdNo' => 1,
            'Delivery_Address1' => 'Lorem ipsum dolor sit amet',
            'Delivery_Address2' => 'Lorem ipsum dolor sit amet',
            'Delivery_Address3' => 'Lorem ipsum dolor sit amet',
            'Vehicle_No' => 'Lorem ipsum dolor sit amet',
            'Narration' => 'Lorem ipsum dolor sit amet',
            'Total_Qty' => 1.5,
            'Total_Bags' => 1,
            'SubTotal_Amount' => 1.5,
            'Total_DiscountAmount' => 1.5,
            'Total_TaxAmount' => 1.5,
            'Gross_Amount' => 1.5,
            'CashDiscount_Perc' => 1.5,
            'CashDiscount_Amount' => 1.5,
            'Assessable_Value' => 1.5,
            'Tax_Perc' => 1.5,
            'Tax_Amount' => 1.5,
            'Freight_Amount' => 1.5,
            'AddLess_Amount' => 1.5,
            'Round_Off' => 1.5,
            'Net_Amount' => 1.5,
            'Document_Through' => 'Lorem ipsum dolor sit amet',
            'Despatch_To' => 'Lorem ipsum dolor sit amet',
            'Lr_No' => 'Lorem ipsum dolor sit amet',
            'Lr_Date' => 'Lorem ipsum dolor ',
            'Booked_By' => 'Lorem ipsum dolor sit amet',
            'Transport_IdNo' => 1,
            'Freight_ToPay_Amount' => 1.5,
            'Dc_No' => 'Lorem ipsum dolor sit amet',
            'Dc_Date' => 'Lorem ipsum dolor sit amet',
            'Order_No' => 'Lorem ipsum dolor sit amet',
            'Order_Date' => 'Lorem ipsum dolor sit amet',
            'Against_CForm_Status' => 1,
            'Entry_Type' => 'Lorem ipsum dolor ',
            'Payment_Terms' => 'Lorem ipsum dolor sit amet',
            'OnAc_IdNo' => 1,
            'Extra_Charges' => 1.5,
            'Total_Extra_Copies' => 1.5,
            'Sub_Total_Copies' => 1.5,
            'Party_Name' => 'Lorem ipsum dolor sit amet',
            'Weight' => 1.5,
            'Sales_OrderAc_IdNo' => 1,
            'Order_Close' => 1,
            'Sales_Order_Selection_Code' => 'Lorem ipsum dolor sit amet',
            'Quotation_No' => 'Lorem ipsum dolor sit amet',
            'Quotation_Date' => 'Lorem ipsum dolor sit amet',
            'Salesman_IdNo' => 1,
            'Removal_Date' => 'Lorem ipsum dolor sit amet',
            'Date_Time_Of_Supply' => 'Lorem ipsum dolor sit amet',
            'Total_DiscountAmount_item' => 1.5,
            'Entry_VAT_GST_Type' => 'Lorem ipsum dolor sit amet',
            'Entry_GST_Tax_Type' => 'Lorem ipsum dolor sit amet',
            'CGst_Amount' => 1.5,
            'SGst_Amount' => 1.5,
            'IGst_Amount' => 1.5,
            'DeliveryTo_IdNo' => 1
        ],
    ];
}
