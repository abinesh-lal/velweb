<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SalesmanHeadFixture
 *
 */
class SalesmanHeadFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'salesman_head';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'Salesman_Idno' => ['type' => 'integer', 'length' => '10', 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Salesman_Name' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Sur_Name' => ['type' => 'string', 'length' => '100', 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'SalesMan_EmailID2' => ['type' => 'string', 'length' => '50', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'SalesMan_EmailID' => ['type' => 'string', 'length' => '50', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Collection_Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Total_Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Total_Quantity' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Month_Idno' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['Salesman_Idno'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'Salesman_Idno' => 1,
            'Salesman_Name' => 'Lorem ipsum dolor sit amet',
            'Sur_Name' => 'Lorem ipsum dolor sit amet',
            'SalesMan_EmailID2' => 'Lorem ipsum dolor sit amet',
            'SalesMan_EmailID' => 'Lorem ipsum dolor sit amet',
            'Collection_Amount' => 1.5,
            'Total_Amount' => 1.5,
            'Total_Quantity' => 1.5,
            'Month_Idno' => 1
        ],
    ];
}
