<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SalesOrderDetailsFixture
 *
 */
class SalesOrderDetailsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'Sales_Order_Code' => ['type' => 'string', 'length' => '50', 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Company_IdNo' => ['type' => 'integer', 'length' => '5', 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Sales_Order_No' => ['type' => 'string', 'length' => '50', 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'for_OrderBy' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => false, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Sales_Order_Date' => ['type' => 'timestamp', 'length' => null, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null],
        'Ledger_IdNo' => ['type' => 'integer', 'length' => '10', 'null' => false, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'SL_No' => ['type' => 'integer', 'length' => '5', 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Item_IdNo' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'ItemGroup_IdNo' => ['type' => 'integer', 'length' => '5', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Unit_IdNo' => ['type' => 'integer', 'length' => '5', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Noof_Items' => ['type' => 'decimal', 'length' => '18', 'precision' => '3', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Bags' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Weight_Bag' => ['type' => 'decimal', 'length' => '18', 'precision' => '3', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Weight' => ['type' => 'decimal', 'length' => '18', 'precision' => '3', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Rate' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Tax_Rate' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Discount_Perc' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Discount_Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Tax_Perc' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Tax_Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Total_Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Bag_Nos' => ['type' => 'string', 'length' => '500', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Serial_No' => ['type' => 'string', 'length' => '500', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Size_IdNo' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Meters' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Colour_IdNo' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Noof_Items_Return' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Sales_Order_Detail_SlNo' => ['type' => 'integer', 'length' => '10', 'autoIncrement' => true, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null],
        'Sales_Items' => ['type' => 'decimal', 'length' => '18', 'precision' => '3', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Sales_Quotation_Code' => ['type' => 'string', 'length' => '50', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Sales_Quotation_Detail_SlNo' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'item_Description' => ['type' => 'string', 'length' => '500', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Entry_Type' => ['type' => 'string', 'length' => '20', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Scheme_Disc_Percentage' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Scheme_Discount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Trade_Disc_Percentage' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Trade_Discount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Cgst_Percentage' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Cgst_Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Sgst_Percentage' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Sgst_Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Igst_Percentage' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Igst_Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Net_Amount' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Total_Rate' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Scheme_UCP' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Discount_Total' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Cash_Discount_Perc_For_All_Item' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Cash_Discount_Amount_For_All_Item' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'HSN_Code' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => '', 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Assessable_Value' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        'Cancel_Quantity' => ['type' => 'decimal', 'length' => '18', 'precision' => '2', 'null' => true, 'default' => '0', 'comment' => null, 'unsigned' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['Sales_Order_Code', 'SL_No'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'Sales_Order_Code' => '3cf2ae48-a21c-4fdf-ae40-fab4647e8e60',
            'Company_IdNo' => 1,
            'Sales_Order_No' => 'Lorem ipsum dolor sit amet',
            'for_OrderBy' => 1.5,
            'Sales_Order_Date' => 1532109319,
            'Ledger_IdNo' => 1,
            'SL_No' => 1,
            'Item_IdNo' => 1,
            'ItemGroup_IdNo' => 1,
            'Unit_IdNo' => 1,
            'Noof_Items' => 1.5,
            'Bags' => 1,
            'Weight_Bag' => 1.5,
            'Weight' => 1.5,
            'Rate' => 1.5,
            'Tax_Rate' => 1.5,
            'Amount' => 1.5,
            'Discount_Perc' => 1.5,
            'Discount_Amount' => 1.5,
            'Tax_Perc' => 1.5,
            'Tax_Amount' => 1.5,
            'Total_Amount' => 1.5,
            'Bag_Nos' => 'Lorem ipsum dolor sit amet',
            'Serial_No' => 'Lorem ipsum dolor sit amet',
            'Size_IdNo' => 1,
            'Meters' => 1.5,
            'Colour_IdNo' => 1,
            'Noof_Items_Return' => 1.5,
            'Sales_Order_Detail_SlNo' => 1,
            'Sales_Items' => 1.5,
            'Sales_Quotation_Code' => 'Lorem ipsum dolor sit amet',
            'Sales_Quotation_Detail_SlNo' => 1,
            'item_Description' => 'Lorem ipsum dolor sit amet',
            'Entry_Type' => 'Lorem ipsum dolor ',
            'Scheme_Disc_Percentage' => 1.5,
            'Scheme_Discount' => 1.5,
            'Trade_Disc_Percentage' => 1.5,
            'Trade_Discount' => 1.5,
            'Cgst_Percentage' => 1.5,
            'Cgst_Amount' => 1.5,
            'Sgst_Percentage' => 1.5,
            'Sgst_Amount' => 1.5,
            'Igst_Percentage' => 1.5,
            'Igst_Amount' => 1.5,
            'Net_Amount' => 1.5,
            'Total_Rate' => 1.5,
            'Scheme_UCP' => 1.5,
            'Discount_Total' => 1.5,
            'Cash_Discount_Perc_For_All_Item' => 1.5,
            'Cash_Discount_Amount_For_All_Item' => 1.5,
            'HSN_Code' => 'Lorem ipsum dolor sit amet',
            'Assessable_Value' => 1.5,
            'Cancel_Quantity' => 1.5
        ],
    ];
}
