<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SalesmanHeadTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SalesmanHeadTable Test Case
 */
class SalesmanHeadTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SalesmanHeadTable
     */
    public $SalesmanHead;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.salesman_head'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SalesmanHead') ? [] : ['className' => 'App\Model\Table\SalesmanHeadTable'];
        $this->SalesmanHead = TableRegistry::get('SalesmanHead', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SalesmanHead);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
