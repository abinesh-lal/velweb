<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CetegoryHeadTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CetegoryHeadTable Test Case
 */
class CetegoryHeadTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CetegoryHeadTable
     */
    public $CetegoryHead;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cetegory_head'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CetegoryHead') ? [] : ['className' => 'App\Model\Table\CetegoryHeadTable'];
        $this->CetegoryHead = TableRegistry::get('CetegoryHead', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CetegoryHead);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
