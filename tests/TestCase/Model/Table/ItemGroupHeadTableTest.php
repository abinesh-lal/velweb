<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ItemGroupHeadTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ItemGroupHeadTable Test Case
 */
class ItemGroupHeadTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ItemGroupHeadTable
     */
    public $ItemGroupHead;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.item_group_head'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ItemGroupHead') ? [] : ['className' => 'App\Model\Table\ItemGroupHeadTable'];
        $this->ItemGroupHead = TableRegistry::get('ItemGroupHead', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ItemGroupHead);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
