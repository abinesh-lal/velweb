<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SalesOrderGstTaxDetailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SalesOrderGstTaxDetailsTable Test Case
 */
class SalesOrderGstTaxDetailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SalesOrderGstTaxDetailsTable
     */
    public $SalesOrderGstTaxDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sales_order_gst_tax_details'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SalesOrderGstTaxDetails') ? [] : ['className' => 'App\Model\Table\SalesOrderGstTaxDetailsTable'];
        $this->SalesOrderGstTaxDetails = TableRegistry::get('SalesOrderGstTaxDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SalesOrderGstTaxDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
