<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SalesOrderHeadTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SalesOrderHeadTable Test Case
 */
class SalesOrderHeadTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SalesOrderHeadTable
     */
    public $SalesOrderHead;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sales_order_head'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SalesOrderHead') ? [] : ['className' => 'App\Model\Table\SalesOrderHeadTable'];
        $this->SalesOrderHead = TableRegistry::get('SalesOrderHead', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SalesOrderHead);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
