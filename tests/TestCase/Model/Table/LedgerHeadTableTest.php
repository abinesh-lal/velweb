<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LedgerHeadTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LedgerHeadTable Test Case
 */
class LedgerHeadTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LedgerHeadTable
     */
    public $LedgerHead;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ledger_head'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('LedgerHead') ? [] : ['className' => 'App\Model\Table\LedgerHeadTable'];
        $this->LedgerHead = TableRegistry::get('LedgerHead', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LedgerHead);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
