<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SalesmanLoginHeadTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SalesmanLoginHeadTable Test Case
 */
class SalesmanLoginHeadTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SalesmanLoginHeadTable
     */
    public $SalesmanLoginHead;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.salesman_login_head'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SalesmanLoginHead') ? [] : ['className' => 'App\Model\Table\SalesmanLoginHeadTable'];
        $this->SalesmanLoginHead = TableRegistry::get('SalesmanLoginHead', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SalesmanLoginHead);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
