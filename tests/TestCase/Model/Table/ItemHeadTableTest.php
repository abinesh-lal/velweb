<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ItemHeadTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ItemHeadTable Test Case
 */
class ItemHeadTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ItemHeadTable
     */
    public $ItemHead;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.item_head'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ItemHead') ? [] : ['className' => 'App\Model\Table\ItemHeadTable'];
        $this->ItemHead = TableRegistry::get('ItemHead', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ItemHead);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
