<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ItemPortfolioHeadTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ItemPortfolioHeadTable Test Case
 */
class ItemPortfolioHeadTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ItemPortfolioHeadTable
     */
    public $ItemPortfolioHead;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.item_portfolio_head'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ItemPortfolioHead') ? [] : ['className' => 'App\Model\Table\ItemPortfolioHeadTable'];
        $this->ItemPortfolioHead = TableRegistry::get('ItemPortfolioHead', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ItemPortfolioHead);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
